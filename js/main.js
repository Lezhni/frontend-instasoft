$(document).ready(function() {
	
	$('header.gl').css({
		width: $(window).innerWidth(),
		height: $(window).innerHeight()
	});
	$('header.gl .container').css({
		marginTop: -$('header.gl .container').height()/2
	});

	$('.callback').click(function() {
		var targ = $(this).attr('href');
		$.magnificPopup.open({
			items: {
				src: targ,
				type: 'inline'
			}
		});
		return false;
	});

	$('.about h3').on('scrollSpy:enter', function() {

		if (!$(this).hasClass('showed')) {
			$({someValue: 60}).animate({someValue: 10}, {
		        duration: 3000,
		        easing:'swing',
		        step: function() {
		            $('.about h3').html(Math.round(this.someValue) + '<span>минут в день требуется для<br>настройки программы</span>');
		        }
		    });
		}
		$(this).addClass('showed');
	});

	$('.advantages .advantage-single').eq(0).on('scrollSpy:enter', function() {
		$('.advantages .advantage-single').addClass('shoved');
	});
	$('.advantages .advantage-single').eq(0).on('scrollSpy:exit', function() {
		$('.advantages .advantage-single').removeClass('shoved');
	});

	$('.functions .functions-list').on('scrollSpy:enter', function() {
		$('.functions .functions-list').addClass('showed');
	});
	$('.functions .functions-list').on('scrollSpy:exit', function() {
		$('.functions .functions-list').removeClass('showed');
	});

	$('.about h3').scrollSpy();
	$('.advantages .advantage-single').eq(0).scrollSpy();
	$('.functions .functions-list').scrollSpy();

	var c = 0;

	$('.reviews-list img, .reviews-list strong').click(function() {

		c = $(this).closest('.review-single').index();
		var review = $(this).closest('.review-single');
		$('.review-single img').removeClass('review-active')
		review.find('img').addClass('review-active');
		$('.review-top-img').attr('src', review.find('img').attr('src'));
		$('.review-top-title').text(review.find('strong').text());
		$('.review-top-desc').text(review.find('p').text());
		$('.review-top-text').text(review.find('.review-text').text());
	});

	setInterval(function() {
		$('.reviews-list .review-single').eq(c).find('img').click();
		if (c === $('.reviews-list .review-single').length) {
			c = 0;
		} else {
			c++;
		}
	}, 4000);

	$('form').submit(function() {
		var form = $(this);
		var fio = form.find('[name=name]').val();
		var email = form.find('[name=email]').val();

		if (email == '' || email == ' ' || fio == '' || fio == ' ') {
			alert('Заполните все поля!');
			return false;
		}
		$.ajax({
			method: 'POST',
			url: 'send.php',
			data: {
				fio: fio,
				email: email
			},
			success: function(data) {
				form.find('[name=name]').val('');
				form.find('[name=email]').val('');

				if (data == 'sended') {
					$.magnificPopup.close();
					$.magnificPopup.open({
						items: {
					    	src: '#thanks',
					    	type: 'inline'
					  	}
					});
				} else {
					alert('Произошла ошибка отправки. Попробуйте еще раз');
				}
			}
		});
		return false;
	}); 

	$('.scroll-me').click(function() {
		$('html, body').animate({
			scrollTop: $('header.gl').height(),
		}, 1500);
	});

});

$(window).on('resize', function() {
	$('header.gl').css({
		width: $(window).innerWidth(),
		height: $(window).innerHeight()
	});
});

$(window).one('scroll', function() {
	if ($(window).scrollTop() < $('header.gl').height()) {
		$('body, html').animate({
			scrollTop: $('header.gl').height()
	    }, 1500);
	}
});